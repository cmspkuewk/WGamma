import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import argparse
import sys
from array import array

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input_filename', help='input_filename', dest='infname', required=True)
parser.add_argument('-o', '--output_filename', help='output_filename', dest='outfname', required=True)
parser.add_argument('-v', '--dset_version', help='dataset version', dest='version', required=True)

args = parser.parse_args()

fin=ROOT.TFile(args.infname,"read")
fout=ROOT.TFile(args.outfname,"recreate")
fin.cd()

def make_new_tree(told):

    #prevents root from wring a lot of stuff into memory
    #tmpfile=TFile("weight_events_tmpfile.root","recreate")
    
    fout.cd()
    
    #gROOT.cd() #in order to prevent the original tree from being added to the output file
    tnew=told.CloneTree()

    dsetversion=ROOT.vector('string')()

    dsetversion.push_back(args.version)

    dsetversion[0] = args.version

    br=tnew.Branch('dsetversion',dsetversion)

    for entry in range(tnew.GetEntries()):
        tnew.GetEntry(entry)

        br.Fill()

    fout.cd()

    tnew.Write()

    #overwrite this file, which can be very large, with a new file, so that it becomes small
    #tmpfile=TFile("weight_events_tmpfile.root","recreate")    

#get the maximum cycle number

maxcycle=0

for key in fin.GetListOfKeys():
    obj=key.ReadObj()
    if obj.GetName() == "Events":
        if key.GetCycle() > maxcycle:
            maxcycle = key.GetCycle()

print "maxcycle = "+str(maxcycle)

for key in fin.GetListOfKeys():
    obj=key.ReadObj()
    if obj.GetName() == "nEvents":
        fout.WriteObject(obj.Clone(),obj.GetName())
    elif obj.GetName() == "nEventsGenWeighted":
        fout.WriteObject(obj.Clone(),obj.GetName())  
    elif obj.GetName() == "Events":
        if key.GetCycle() != maxcycle:
            continue
        make_new_tree(obj)
