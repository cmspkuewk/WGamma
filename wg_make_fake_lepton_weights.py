#this script should be used with ROOT version 6.14 or greater in order to get the RDataFrame
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--nthreads',dest='nthreads',default=0) #the argument to EnableImplicitMT
parser.add_argument('--workdir',dest='workdir',default='/eos/user/a/amlevin')
parser.add_argument('--year',dest='year',default='2016')
parser.add_argument('-o',dest='outputdir',default="/eos/user/a/amlevin/www/tmp/")

args = parser.parse_args()

import math

import json

import sys
import os

#otherwise, root will parse the command line options, see here http://root.cern.ch/phpBB3/viewtopic.php?f=14&t=18637
sys.argv = []

import ROOT

from array import array

ROOT.gStyle.SetOptStat(0)

if args.year == "2016":
    lumi = 35.9
elif args.year == "2017":
    lumi = 41.5
elif args.year == "2018":
    lumi = 59.6
else:
    assert(0)

felectronout=ROOT.TFile("electron_"+args.year+"_frs.root","recreate")
fmuonout=ROOT.TFile("muon_"+args.year+"_frs.root","recreate")

#electron_data_samples = []

if args.year == "2016" or args.year == "2017":
    electron_data_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/single_electron_fake_lepton.root"}]
elif args.year == "2018":
    electron_data_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/egamma_fake_lepton.root"}]
else:
    assert(0)

#electron_mc_samples = []

electron_mc_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/wjets_fake_lepton.root", "xs" : 60430.0, "subtract" : True, "conversions" : False},{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/zjets_fake_lepton.root", "xs" : 4963.0, "subtract" : True, "conversions" : False},{'filename' : args.workdir + '/data/wg/'+args.year+'/1June2019/gjetsht40to100_fake_lepton.root', 'xs' : 20660.0, 'subtract' : True, 'conversions' : True}, {'filename' : args.workdir + '/data/wg/'+args.year+'/1June2019/gjetsht100to200_fake_lepton.root'  , 'xs' : 9249.0, 'subtract' : True, 'conversions' : True}, {'filename' : args.workdir + '/data/wg/'+args.year+'/1June2019/gjetsht200to400_fake_lepton.root' , 'xs' : 2321.0, 'subtract' : True, 'conversions' : True}, {'filename' : args.workdir + '/data/wg/'+args.year+'/1June2019/gjetsht400to600_fake_lepton.root', 'xs' : 275.2, 'subtract' : True, 'conversions' : True}, {'filename' : args.workdir + '/data/wg/'+args.year+'/1June2019/gjetsht600toinf_fake_lepton.root', 'xs' : 93.19, 'subtract' :True, 'conversions' : True}]

#electron_mc_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/wjets_fake_lepton.root", "xs" : 60430.0, "subtract" : True, "conversions" : False},{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/zjets_fake_lepton.root", "xs" : 4963.0, "subtract" : True, "conversions" : False}]

#electron_mc_samples = [{"filename" : "/eos/user/a/aera/andrew/data/wg/2016/qcd_bctoe_170250.root", "xs" : 2608, "subtract" : False},{"filename" : "/eos/user/a/aera/andrew/data/wg/2016/qcd_bctoe_2030.root", "xs" : 363100, "subtract" : False},{"filename" : "/eos/user/a/aera/andrew/data/wg/2016/qcd_bctoe_250.root", "xs" : 722.6, "subtract" : False},{"filename" : "/eos/user/a/aera/andrew/data/wg/2016/qcd_bctoe_3080.root", "xs" : 417800, "subtract" : False},{"filename" : "/eos/user/a/aera/andrew/data/wg/2016/qcd_bctoe_80170.root", "xs" : 39860, "subtract" : False}]

muon_data_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/single_muon_fake_lepton.root"}]

#muon_data_samples = []

#muon_mc_samples = []

muon_mc_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/wjets_fake_lepton.root", "xs" : 60430.0, "subtract" : True},{"filename" : args.workdir + "/data/wg/" +args.year +"/1June2019/zjets_fake_lepton.root", "xs" : 4963.0, "subtract" : True}]
#muon_mc_samples = [{"filename" : args.workdir + "/data/wg/" +args.year+"/1June2019/wjets_fake_lepton.root", "xs" : 60430.0, "subtract" : True}]
#muon_mc_samples = [{"filename" : args.workdir + "/data/wg/" +args.year +"/1June2019/zjets_fake_lepton.root", "xs" : 4963.0, "subtract" : True}]

electron_ptbins=array('d', [30,40,50])
electron_etabins=array('d', [0,0.5,1,1.479,2.0,2.5])

electron_highest_pt_bin_center=(electron_ptbins[len(electron_ptbins)-1]+electron_ptbins[len(electron_ptbins)-2])/2

electron_hist_eta_pt_model=ROOT.RDF.TH2DModel("","",len(electron_etabins)-1,electron_etabins,len(electron_ptbins)-1,electron_ptbins)

electron_hist_eta_model=ROOT.RDF.TH1DModel("","",len(electron_etabins)-1,electron_etabins)

electron_hist_pt_model=ROOT.RDF.TH1DModel("","",len(electron_ptbins)-1,electron_ptbins)

loose_electron_hist_eta_pt=electron_hist_eta_pt_model.GetHistogram()
tight_electron_hist_eta_pt=electron_hist_eta_pt_model.GetHistogram()
data_loose_electron_hist_eta_pt=electron_hist_eta_pt_model.GetHistogram()
data_tight_electron_hist_eta_pt=electron_hist_eta_pt_model.GetHistogram()
mc_loose_electron_hist_eta_pt=electron_hist_eta_pt_model.GetHistogram()
mc_tight_electron_hist_eta_pt=electron_hist_eta_pt_model.GetHistogram()

loose_electron_hist_pt=electron_hist_pt_model.GetHistogram()
tight_electron_hist_pt=electron_hist_pt_model.GetHistogram()
data_loose_electron_hist_pt=electron_hist_pt_model.GetHistogram()
data_tight_electron_hist_pt=electron_hist_pt_model.GetHistogram()
mc_loose_electron_hist_pt=electron_hist_pt_model.GetHistogram()
mc_tight_electron_hist_pt=electron_hist_pt_model.GetHistogram()

loose_electron_hist_eta=electron_hist_eta_model.GetHistogram()
tight_electron_hist_eta=electron_hist_eta_model.GetHistogram()
data_loose_electron_hist_eta=electron_hist_eta_model.GetHistogram()
data_tight_electron_hist_eta=electron_hist_eta_model.GetHistogram()
mc_loose_electron_hist_eta=electron_hist_eta_model.GetHistogram()
mc_tight_electron_hist_eta=electron_hist_eta_model.GetHistogram()

loose_electron_hist_eta_pt.Sumw2()
tight_electron_hist_eta_pt.Sumw2()
data_loose_electron_hist_eta_pt.Sumw2()
data_tight_electron_hist_eta_pt.Sumw2()
mc_loose_electron_hist_eta_pt.Sumw2()
mc_tight_electron_hist_eta_pt.Sumw2()

loose_electron_hist_pt.Sumw2()
tight_electron_hist_pt.Sumw2()
data_loose_electron_hist_pt.Sumw2()
data_tight_electron_hist_pt.Sumw2()
mc_loose_electron_hist_pt.Sumw2()
mc_tight_electron_hist_pt.Sumw2()

loose_electron_hist_eta.Sumw2()
tight_electron_hist_eta.Sumw2()
data_loose_electron_hist_eta.Sumw2()
data_tight_electron_hist_eta.Sumw2()
mc_loose_electron_hist_eta.Sumw2()
mc_tight_electron_hist_eta.Sumw2()

muon_ptbins=array('d', [25,30,40,50])    
muon_etabins=array('d', [0,0.5,1.0,1.479,2.0,2.5])

muon_highest_pt_bin_center=(muon_ptbins[len(muon_ptbins)-1]+muon_ptbins[len(muon_ptbins)-2])/2

muon_hist_eta_pt_model=ROOT.RDF.TH2DModel("","",len(muon_etabins)-1,muon_etabins,len(muon_ptbins)-1,muon_ptbins)

muon_hist_eta_model=ROOT.RDF.TH1DModel("","",len(muon_etabins)-1,muon_etabins)

muon_hist_pt_model=ROOT.RDF.TH1DModel("","",len(muon_ptbins)-1,muon_ptbins)

loose_muon_hist_eta_pt=muon_hist_eta_pt_model.GetHistogram()
tight_muon_hist_eta_pt=muon_hist_eta_pt_model.GetHistogram()
data_loose_muon_hist_eta_pt=muon_hist_eta_pt_model.GetHistogram()
data_tight_muon_hist_eta_pt=muon_hist_eta_pt_model.GetHistogram()
mc_loose_muon_hist_eta_pt=muon_hist_eta_pt_model.GetHistogram()
mc_tight_muon_hist_eta_pt=muon_hist_eta_pt_model.GetHistogram()

loose_muon_hist_pt=muon_hist_pt_model.GetHistogram()
tight_muon_hist_pt=muon_hist_pt_model.GetHistogram()
data_loose_muon_hist_pt=muon_hist_pt_model.GetHistogram()
data_tight_muon_hist_pt=muon_hist_pt_model.GetHistogram()
mc_loose_muon_hist_pt=muon_hist_pt_model.GetHistogram()
mc_tight_muon_hist_pt=muon_hist_pt_model.GetHistogram()

loose_muon_hist_eta=muon_hist_eta_model.GetHistogram()
tight_muon_hist_eta=muon_hist_eta_model.GetHistogram()
data_loose_muon_hist_eta=muon_hist_eta_model.GetHistogram()
data_tight_muon_hist_eta=muon_hist_eta_model.GetHistogram()
mc_loose_muon_hist_eta=muon_hist_eta_model.GetHistogram()
mc_tight_muon_hist_eta=muon_hist_eta_model.GetHistogram()

loose_muon_hist_eta_pt.Sumw2()
tight_muon_hist_eta_pt.Sumw2()
data_loose_muon_hist_eta_pt.Sumw2()
data_tight_muon_hist_eta_pt.Sumw2()
mc_loose_muon_hist_eta_pt.Sumw2()
mc_tight_muon_hist_eta_pt.Sumw2()

loose_muon_hist_pt.Sumw2()
tight_muon_hist_pt.Sumw2()
data_loose_muon_hist_pt.Sumw2()
data_tight_muon_hist_pt.Sumw2()
mc_loose_muon_hist_pt.Sumw2()
mc_tight_muon_hist_pt.Sumw2()

loose_muon_hist_eta.Sumw2()
tight_muon_hist_eta.Sumw2()
data_loose_muon_hist_eta.Sumw2()
data_tight_muon_hist_eta.Sumw2()
mc_loose_muon_hist_eta.Sumw2()
mc_tight_muon_hist_eta.Sumw2()

ROOT.ROOT.EnableImplicitMT(args.nthreads)

print "Processing electron data samples"

for sample in electron_data_samples:
    rdf=ROOT.RDataFrame("Events",sample["filename"])

    rdf = rdf.Filter("puppimet < 20 && puppimt < 20 && abs(lepton_pdgid) == 11 && (hlt & 1) && jet_pt > 30 && lepton_jet_dr > 0.3") 

    rdf_pass=rdf.Filter("is_lepton_tight").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(electron_highest_pt_bin_center)+" ? lepton_pt : "+str(electron_highest_pt_bin_center))
    rdf_fail=rdf.Filter("!is_lepton_tight").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(electron_highest_pt_bin_center)+" ? lepton_pt : "+str(electron_highest_pt_bin_center))

    h_pass_eta_pt=rdf_pass.Histo2D(electron_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt")
    h_fail_eta_pt=rdf_fail.Histo2D(electron_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt")
    h_pass_pt=rdf_pass.Histo1D(electron_hist_pt_model,"cropped_lepton_pt")
    h_fail_pt=rdf_fail.Histo1D(electron_hist_pt_model,"cropped_lepton_pt")
    h_pass_eta=rdf_pass.Histo1D(electron_hist_eta_model,"abs_lepton_eta")
    h_fail_eta=rdf_fail.Histo1D(electron_hist_eta_model,"abs_lepton_eta")

    tight_electron_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    loose_electron_hist_eta_pt.Add(h_fail_eta_pt.GetValue())
    data_tight_electron_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    data_loose_electron_hist_eta_pt.Add(h_fail_eta_pt.GetValue())

    tight_electron_hist_pt.Add(h_pass_pt.GetValue())
    loose_electron_hist_pt.Add(h_fail_pt.GetValue())
    data_tight_electron_hist_pt.Add(h_pass_pt.GetValue())
    data_loose_electron_hist_pt.Add(h_fail_pt.GetValue())

    tight_electron_hist_eta.Add(h_pass_eta.GetValue())
    loose_electron_hist_eta.Add(h_fail_eta.GetValue())
    data_tight_electron_hist_eta.Add(h_pass_eta.GetValue())
    data_loose_electron_hist_eta.Add(h_fail_eta.GetValue())

print "Processing electron MC samples"

for sample in electron_mc_samples:
    f = ROOT.TFile.Open(sample["filename"])
    t = f.Get("Events")
    n_weighted_events = f.Get("nEventsGenWeighted").GetBinContent(1)

    if sample["subtract"]:
        sign = "-"
    else:
        sign = "+"

    rdf=ROOT.RDataFrame("Events",sample["filename"])

    if sample["conversions"]:
        lepton_gen_matching = "(lepton_gen_matching == 22)"
    else:
        lepton_gen_matching = "(lepton_gen_matching == 1 || lepton_gen_matching == 15)"

    rdf=rdf.Filter(lepton_gen_matching + " && puppimet < 20 && puppimt < 20 && abs(lepton_pdgid) == 11 && (hlt & 1) && jet_pt > 30 && lepton_jet_dr > 0.3")

    rdf_pass=rdf.Filter("is_lepton_tight").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(electron_highest_pt_bin_center)+" ? lepton_pt : "+str(electron_highest_pt_bin_center)).Define("weight",sign+str(sample["xs"]*1000*lumi/n_weighted_events)+"*gen_weight/abs(gen_weight)")
    rdf_fail=rdf.Filter("!is_lepton_tight").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(electron_highest_pt_bin_center)+" ? lepton_pt : "+str(electron_highest_pt_bin_center)).Define("weight",sign+str(sample["xs"]*1000*lumi/n_weighted_events)+"*gen_weight/abs(gen_weight)")

    h_pass_eta_pt=rdf_pass.Histo2D(electron_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt","weight")
    h_fail_eta_pt=rdf_fail.Histo2D(electron_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt","weight")
    h_pass_pt=rdf_pass.Histo1D(electron_hist_pt_model,"cropped_lepton_pt","weight")
    h_fail_pt=rdf_fail.Histo1D(electron_hist_pt_model,"cropped_lepton_pt","weight")
    h_pass_eta=rdf_pass.Histo1D(electron_hist_eta_model,"abs_lepton_eta","weight")
    h_fail_eta=rdf_fail.Histo1D(electron_hist_eta_model,"abs_lepton_eta","weight")

    tight_electron_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    loose_electron_hist_eta_pt.Add(h_fail_eta_pt.GetValue())
    mc_tight_electron_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    mc_loose_electron_hist_eta_pt.Add(h_fail_eta_pt.GetValue())

    tight_electron_hist_pt.Add(h_pass_pt.GetValue())
    loose_electron_hist_pt.Add(h_fail_pt.GetValue())
    mc_tight_electron_hist_pt.Add(h_pass_pt.GetValue())
    mc_loose_electron_hist_pt.Add(h_fail_pt.GetValue())

    tight_electron_hist_eta.Add(h_pass_eta.GetValue())
    loose_electron_hist_eta.Add(h_fail_eta.GetValue())
    mc_tight_electron_hist_eta.Add(h_pass_eta.GetValue())
    mc_loose_electron_hist_eta.Add(h_fail_eta.GetValue())

    f.Close()

print "Processing muon data samples"

for sample in muon_data_samples:
    rdf=ROOT.RDataFrame("Events",sample["filename"])

    rdf=rdf.Filter("puppimet < 20 && puppimt < 20 && abs(lepton_pdgid) == 13 && (hlt & 1) && jet_pt > 30 && lepton_jet_dr > 0.3")

    rdf_pass=rdf.Filter("lepton_pfRelIso04_all < 0.15").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(muon_highest_pt_bin_center)+" ? lepton_pt : "+str(muon_highest_pt_bin_center))
    rdf_fail=rdf.Filter("0.2 < lepton_pfRelIso04_all && lepton_pfRelIso04_all < 0.4").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(muon_highest_pt_bin_center)+" ? lepton_pt : "+str(muon_highest_pt_bin_center))

    h_pass_eta_pt=rdf_pass.Histo2D(muon_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt")
    h_fail_eta_pt=rdf_fail.Histo2D(muon_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt")
    h_pass_pt=rdf_pass.Histo1D(muon_hist_pt_model,"cropped_lepton_pt")
    h_fail_pt=rdf_fail.Histo1D(muon_hist_pt_model,"cropped_lepton_pt")
    h_pass_eta=rdf_pass.Histo1D(muon_hist_eta_model,"abs_lepton_eta")
    h_fail_eta=rdf_fail.Histo1D(muon_hist_eta_model,"abs_lepton_eta")

    tight_muon_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    loose_muon_hist_eta_pt.Add(h_fail_eta_pt.GetValue())
    data_tight_muon_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    data_loose_muon_hist_eta_pt.Add(h_fail_eta_pt.GetValue())

    tight_muon_hist_pt.Add(h_pass_pt.GetValue())
    loose_muon_hist_pt.Add(h_fail_pt.GetValue())
    data_tight_muon_hist_pt.Add(h_pass_pt.GetValue())
    data_loose_muon_hist_pt.Add(h_fail_pt.GetValue())

    tight_muon_hist_eta.Add(h_pass_eta.GetValue())
    loose_muon_hist_eta.Add(h_fail_eta.GetValue())
    data_tight_muon_hist_eta.Add(h_pass_eta.GetValue())
    data_loose_muon_hist_eta.Add(h_fail_eta.GetValue())

print "Processing muon MC samples"

for sample in muon_mc_samples:
    f = ROOT.TFile.Open(sample["filename"])
    t = f.Get("Events")
    n_weighted_events = f.Get("nEventsGenWeighted").GetBinContent(1)

    if sample["subtract"]:
        sign = "-"
    else:
        sign = "+"

    rdf=ROOT.RDataFrame("Events",sample["filename"])

    rdf=rdf.Filter("puppimet < 20 && puppimt < 20 && abs(lepton_pdgid) == 13 && (hlt & 1) && jet_pt > 30 && lepton_jet_dr > 0.3")

    rdf_pass=rdf.Filter("lepton_pfRelIso04_all < 0.15").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(muon_highest_pt_bin_center)+" ? lepton_pt : "+str(muon_highest_pt_bin_center)).Define("weight",sign+str(sample["xs"]*1000*lumi/n_weighted_events)+"*gen_weight/abs(gen_weight)")
    rdf_fail=rdf.Filter("0.2 < lepton_pfRelIso04_all && lepton_pfRelIso04_all < 0.4").Define("abs_lepton_eta","abs(lepton_eta)").Define("cropped_lepton_pt","lepton_pt < "+str(muon_highest_pt_bin_center)+" ? lepton_pt : "+str(muon_highest_pt_bin_center)).Define("weight",sign+str(sample["xs"]*1000*lumi/n_weighted_events)+"*gen_weight/abs(gen_weight)")

    h_pass_eta_pt=rdf_pass.Histo2D(muon_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt","weight")
    h_fail_eta_pt=rdf_fail.Histo2D(muon_hist_eta_pt_model,"abs_lepton_eta","cropped_lepton_pt","weight")
    h_pass_pt=rdf_pass.Histo1D(muon_hist_pt_model,"cropped_lepton_pt","weight")
    h_fail_pt=rdf_fail.Histo1D(muon_hist_pt_model,"cropped_lepton_pt","weight")
    h_pass_eta=rdf_pass.Histo1D(muon_hist_eta_model,"abs_lepton_eta","weight")
    h_fail_eta=rdf_fail.Histo1D(muon_hist_eta_model,"abs_lepton_eta","weight")

    tight_muon_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    loose_muon_hist_eta_pt.Add(h_fail_eta_pt.GetValue())
    mc_tight_muon_hist_eta_pt.Add(h_pass_eta_pt.GetValue())
    mc_loose_muon_hist_eta_pt.Add(h_fail_eta_pt.GetValue())

    tight_muon_hist_pt.Add(h_pass_pt.GetValue())
    loose_muon_hist_pt.Add(h_fail_pt.GetValue())
    mc_tight_muon_hist_pt.Add(h_pass_pt.GetValue())
    mc_loose_muon_hist_pt.Add(h_fail_pt.GetValue())

    tight_muon_hist_eta.Add(h_pass_eta.GetValue())
    loose_muon_hist_eta.Add(h_fail_eta.GetValue())
    mc_tight_muon_hist_eta.Add(h_pass_eta.GetValue())
    mc_loose_muon_hist_eta.Add(h_fail_eta.GetValue())

    f.Close()

mc_data_ratio_tight_electron_hist_eta_pt = electron_hist_eta_pt_model.GetHistogram()
mc_data_ratio_loose_electron_hist_eta_pt = electron_hist_eta_pt_model.GetHistogram()

mc_data_ratio_tight_electron_hist_eta_pt.Add(mc_tight_electron_hist_eta_pt)
mc_data_ratio_tight_electron_hist_eta_pt.Divide(data_tight_electron_hist_eta_pt)

mc_data_ratio_tight_electron_hist_eta_pt.Scale(-1)

mc_data_ratio_loose_electron_hist_eta_pt.Add(mc_loose_electron_hist_eta_pt)
mc_data_ratio_loose_electron_hist_eta_pt.Divide(data_loose_electron_hist_eta_pt)

mc_data_ratio_loose_electron_hist_eta_pt.Scale(-1)

mc_data_ratio_tight_muon_hist_eta_pt = muon_hist_eta_pt_model.GetHistogram()
mc_data_ratio_loose_muon_hist_eta_pt = muon_hist_eta_pt_model.GetHistogram()

mc_data_ratio_tight_muon_hist_eta_pt.Add(mc_tight_muon_hist_eta_pt)
mc_data_ratio_tight_muon_hist_eta_pt.Divide(data_tight_muon_hist_eta_pt)

mc_data_ratio_tight_muon_hist_eta_pt.Scale(-1)

mc_data_ratio_loose_muon_hist_eta_pt.Add(mc_loose_muon_hist_eta_pt)
mc_data_ratio_loose_muon_hist_eta_pt.Divide(data_loose_muon_hist_eta_pt)

mc_data_ratio_loose_muon_hist_eta_pt.Scale(-1)

mc_data_ratio_tight_electron_hist_pt = electron_hist_pt_model.GetHistogram()
mc_data_ratio_loose_electron_hist_pt = electron_hist_pt_model.GetHistogram()

mc_data_ratio_tight_electron_hist_pt.Add(mc_tight_electron_hist_pt)
mc_data_ratio_tight_electron_hist_pt.Divide(data_tight_electron_hist_pt)

mc_data_ratio_tight_electron_hist_pt.Scale(-1)

mc_data_ratio_loose_electron_hist_pt.Add(mc_loose_electron_hist_pt)
mc_data_ratio_loose_electron_hist_pt.Divide(data_loose_electron_hist_pt)

mc_data_ratio_loose_electron_hist_pt.Scale(-1)

mc_data_ratio_tight_muon_hist_pt = muon_hist_pt_model.GetHistogram()
mc_data_ratio_loose_muon_hist_pt = muon_hist_pt_model.GetHistogram()

mc_data_ratio_tight_muon_hist_pt.Add(mc_tight_muon_hist_pt)
mc_data_ratio_tight_muon_hist_pt.Divide(data_tight_muon_hist_pt)

mc_data_ratio_tight_muon_hist_pt.Scale(-1)

mc_data_ratio_loose_muon_hist_pt.Add(mc_loose_muon_hist_pt)
mc_data_ratio_loose_muon_hist_pt.Divide(data_loose_muon_hist_pt)

mc_data_ratio_loose_muon_hist_pt.Scale(-1)

mc_data_ratio_tight_electron_hist_eta = electron_hist_eta_model.GetHistogram()
mc_data_ratio_loose_electron_hist_eta = electron_hist_eta_model.GetHistogram()

mc_data_ratio_tight_electron_hist_eta.Add(mc_tight_electron_hist_eta)
mc_data_ratio_tight_electron_hist_eta.Divide(data_tight_electron_hist_eta)

mc_data_ratio_tight_electron_hist_eta.Scale(-1)

mc_data_ratio_loose_electron_hist_eta.Add(mc_loose_electron_hist_eta)
mc_data_ratio_loose_electron_hist_eta.Divide(data_loose_electron_hist_eta)

mc_data_ratio_loose_electron_hist_eta.Scale(-1)

mc_data_ratio_tight_muon_hist_eta = muon_hist_eta_model.GetHistogram()
mc_data_ratio_loose_muon_hist_eta = muon_hist_eta_model.GetHistogram()

mc_data_ratio_tight_muon_hist_eta.Add(mc_tight_muon_hist_eta)
mc_data_ratio_tight_muon_hist_eta.Divide(data_tight_muon_hist_eta)

mc_data_ratio_tight_muon_hist_eta.Scale(-1)

mc_data_ratio_loose_muon_hist_eta.Add(mc_loose_muon_hist_eta)
mc_data_ratio_loose_muon_hist_eta.Divide(data_loose_muon_hist_eta)

mc_data_ratio_loose_muon_hist_eta.Scale(-1)

#mc_data_ratio_tight_muon_hist_pt.Print("all")

def make_plot(hist,xaxislabel,filename):

    c = ROOT.TCanvas()

    hist.GetXaxis().SetTitle(xaxislabel)

    hist.SetMinimum(0)
    hist.Draw("")

    c.SaveAs(args.outputdir + "/" + filename)

make_plot(mc_data_ratio_tight_muon_hist_pt,"p_{T} (GeV)","muon_"+args.year+"_tight_ewk_cont_pt.png")
make_plot(mc_data_ratio_loose_muon_hist_pt,"p_{T} (GeV)","muon_"+args.year+"_loose_ewk_cont_pt.png")
make_plot(mc_data_ratio_tight_muon_hist_pt,"p_{T} (GeV)","muon_"+args.year+"_tight_ewk_cont_pt.png")
make_plot(mc_data_ratio_loose_muon_hist_pt,"p_{T} (GeV)","muon_"+args.year+"_loose_ewk_cont_pt.png")

make_plot(mc_data_ratio_tight_muon_hist_eta,"|\eta|","muon_"+args.year+"_tight_ewk_cont_eta.png")
make_plot(mc_data_ratio_loose_muon_hist_eta,"|\eta|","muon_"+args.year+"_loose_ewk_cont_eta.png")
make_plot(mc_data_ratio_tight_muon_hist_eta,"|\eta|","muon_"+args.year+"_tight_ewk_cont_eta.png")
make_plot(mc_data_ratio_loose_muon_hist_eta,"|\eta|","muon_"+args.year+"_loose_ewk_cont_eta.png")

make_plot(mc_data_ratio_tight_electron_hist_pt,"p_{T} (GeV)","electron_"+args.year+"_tight_ewk_cont_pt.png")
make_plot(mc_data_ratio_loose_electron_hist_pt,"p_{T} (GeV)","electron_"+args.year+"_loose_ewk_cont_pt.png")
make_plot(mc_data_ratio_tight_electron_hist_pt,"p_{T} (GeV)","electron_"+args.year+"_tight_ewk_cont_pt.png")
make_plot(mc_data_ratio_loose_electron_hist_pt,"p_{T} (GeV)","electron_"+args.year+"_loose_ewk_cont_pt.png")

make_plot(mc_data_ratio_tight_electron_hist_eta,"|\eta|","electron_"+args.year+"_tight_ewk_cont_eta.png")
make_plot(mc_data_ratio_loose_electron_hist_eta,"|\eta|","electron_"+args.year+"_loose_ewk_cont_eta.png")
make_plot(mc_data_ratio_tight_electron_hist_eta,"|\eta|","electron_"+args.year+"_tight_ewk_cont_eta.png")
make_plot(mc_data_ratio_loose_electron_hist_eta,"|\eta|","electron_"+args.year+"_loose_ewk_cont_eta.png")

felectronout.cd()

data_tight_electron_hist_eta_pt.Clone("data_tight_electron").Write()
data_loose_electron_hist_eta_pt.Clone("data_loose_electron").Write()

mc_tight_electron_hist_eta_pt.Clone("mc_tight_electron").Write()
mc_loose_electron_hist_eta_pt.Clone("mc_loose_electron").Write()

mc_data_ratio_tight_electron_hist_eta_pt.Clone("mc_data_ratio_tight_electron_hist").Write()
mc_data_ratio_loose_electron_hist_eta_pt.Clone("mc_data_ratio_loose_electron_hist").Write()

tight_electron_hist_eta_pt.Clone("tight_electron").Write()
loose_electron_hist_eta_pt.Clone("loose_electron").Write()

loose_electron_hist_eta_pt.Add(tight_electron_hist_eta_pt)
    
tight_electron_hist_eta_pt.Divide(loose_electron_hist_eta_pt)
tight_electron_hist_eta_pt.Clone("electron_frs").Write()

fmuonout.cd()

data_tight_muon_hist_eta_pt.Clone("data_tight_muon").Write()
data_loose_muon_hist_eta_pt.Clone("data_loose_muon").Write()

mc_tight_muon_hist_eta_pt.Clone("mc_tight_muon").Write()
mc_loose_muon_hist_eta_pt.Clone("mc_loose_muon").Write()

mc_data_ratio_tight_muon_hist_eta_pt.Clone("mc_data_ratio_tight_muon_hist").Write()
mc_data_ratio_loose_muon_hist_eta_pt.Clone("mc_data_ratio_loose_muon_hist").Write()

tight_muon_hist_eta_pt.Clone("tight_muon").Write()
loose_muon_hist_eta_pt.Clone("loose_muon").Write()

loose_muon_hist_eta_pt.Add(tight_muon_hist_eta_pt)
    
tight_muon_hist_eta_pt.Divide(loose_muon_hist_eta_pt)
tight_muon_hist_eta_pt.Clone("muon_frs").Write()
