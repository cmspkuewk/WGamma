import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--workdir',dest='workdir',default='/eos/user/a/amlevin/')

args = parser.parse_args()

import math

mg5amc_nlo_xs = 178.6

mg5amc_wgjets_filename = args.workdir+"/data/wg/2016/1June2019/wgjets.root"

mg5amc_wgjets_file = ROOT.TFile(mg5amc_wgjets_filename)

n_weighted_mg5amc_pass_fid_selection = mg5amc_wgjets_file.Get("nEventsGenWeighted_PassFidSelection").GetBinContent(1)

n_weighted_mg5amc = mg5amc_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)

mg5amc_nlo_fiducial_xs = mg5amc_nlo_xs*n_weighted_mg5amc_pass_fid_selection/n_weighted_mg5amc

#text2hdf5.py ~/wg/datacards/el_mu/wg_dcard_theory_exp.txt -o wg_dcard_theory_exp.hdf5
#combinetf.py wg_dcard_theory_exp.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 --binByBinStat
#text2hdf5.py ~/wg/datacards/el_mu/wg_dcard_theory.txt -o wg_dcard_theory.hdf5 
#combinetf.py wg_dcard_theory.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 
#text2hdf5.py ~/wg/datacards/el_mu/wg_dcard_exp.txt -o wg_dcard_exp.hdf5
#combinetf.py wg_dcard_exp.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 --binByBinStat
#text2hdf5.py ~/wg/datacards/el_mu/wg_dcard_theory_exp.txt -S 0 -o wg_dcard_no_syst.hdf5
#combinetf.py wg_dcard_no_syst.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 
rexp = 1  
rexpunc = 0.045374
rexptheoryunc = 0.006660
rexpexpunc = 0.044420
rexpnosystunc = 0.003161

#text2hdf5.py ~/wg/datacards/el/wg_dcard_theory_exp_el_chan.txt -o wg_dcard_theory_exp_el_chan.hdf5
#combinetf.py wg_dcard_theory_exp_el_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 --binByBinStat
#text2hdf5.py ~/wg/datacards/el/wg_dcard_theory_el_chan.txt -o wg_dcard_theory_el_chan.hdf5
#combinetf.py wg_dcard_theory_el_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1
#text2hdf5.py ~/wg/datacards/el/wg_dcard_exp_el_chan.txt -o wg_dcard_exp_el_chan.hdf5
#combinetf.py wg_dcard_exp_el_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 --binByBinStat
#text2hdf5.py ~/wg/datacards/el/wg_dcard_theory_exp_el_chan.txt -S 0  -o wg_dcard_no_syst_el_chan.hdf5 
#combinetf.py wg_dcard_no_syst_el_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1
rexpelectron = 1  
rexpelectronunc = 0.066682
rexpelectrontheoryunc = 0.011795
rexpelectronexpunc = 0.063421
rexpelectronnosystunc = 0.005748

#text2hdf5.py ~/wg/datacards/mu/wg_dcard_theory_exp_mu_chan.txt -o wg_dcard_theory_exp_mu_chan.hdf5
#combinetf.py wg_dcard_theory_exp_mu_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 --binByBinStat
#text2hdf5.py ~/wg/datacards/mu/wg_dcard_theory_mu_chan.txt -o wg_dcard_theory_mu_chan.hdf5
#combinetf.py wg_dcard_theory_mu_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1
#text2hdf5.py ~/wg/datacards/mu/wg_dcard_exp_mu_chan.txt -o wg_dcard_exp_mu_chan.hdf5
#combinetf.py wg_dcard_exp_mu_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1 --binByBinStat
#text2hdf5.py ~/wg/datacards/mu/wg_dcard_theory_exp_mu_chan.txt -S 0 -o wg_dcard_no_syst_mu_chan.hdf5
#combinetf.py wg_dcard_no_syst_mu_chan.hdf5 --useSciPyMinimizer -t -1 --expectSignal=1
rexpmuon = 1  
rexpmuonunc = 0.053160
rexpmuontheoryunc = 0.007876
rexpmuonexpunc = 0.052570
rexpmuonnosystunc = 0.003750

#text2hdf5.py ~/wg/datacards/el_mu/wg_dcard_theory_exp.txt -o wg_dcard_theory_exp.hdf5
#./combinetf.py wg_dcard_theory_exp.hdf5 --useSciPyMinimizer --binByBinStat
#./combinetf.py wg_dcard_theory_exp.hdf5 --useSciPyMinimizer --binByBinStat --freezetheorysysts --fitresults fitresults_123456789.root
#./combinetf.py wg_dcard_theory_exp.hdf5 --useSciPyMinimizer --binByBinStat
#./combinetf.py wg_dcard_theory_exp.hdf5 --useSciPyMinimizer --freezesysts --fitresults fitresults_123456789.root
robs = 1.009229
robsunc = 0.048640
robsexpunc = 0.047700
robsnosystunc = 0.003383 

#text2hdf5.py ~/wg/datacards/mu/wg_dcard_theory_exp_mu_chan.txt -o wg_dcard_theory_exp_mu_chan.hdf5
#./combinetf.py wg_dcard_theory_exp_mu_chan.hdf5 --useSciPyMinimizer --binByBinStat
#./combinetf.py wg_dcard_theory_exp_mu_chan.hdf5 --useSciPyMinimizer --binByBinStat --freezetheorysysts --fitresults fitresults_123456789.root
#./combinetf.py wg_dcard_theory_exp_mu_chan.hdf5 --useSciPyMinimizer --binByBinStat
#./combinetf.py wg_dcard_theory_exp_mu_chan.hdf5 --useSciPyMinimizer --freezesysts --fitresults fitresults_123456789.root
robsmuon = 1.021679
robsmuonunc = 0.057424
robsmuonexpunc = 0.056895
robsmuonnosystunc = 0.003869

#text2hdf5.py ~/wg/datacards/el/wg_dcard_theory_exp_el_chan.txt -o wg_dcard_theory_exp_el_chan.hdf5
#./combinetf.py wg_dcard_theory_exp_el_chan.hdf5 --useSciPyMinimizer --binByBinStat
#./combinetf.py wg_dcard_theory_exp_el_chan.hdf5 --useSciPyMinimizer --binByBinStat --freezetheorysysts --fitresults fitresults_123456789.root
#./combinetf.py wg_dcard_theory_exp_el_chan.hdf5 --useSciPyMinimizer --binByBinStat
#./combinetf.py wg_dcard_theory_exp_el_chan.hdf5 --useSciPyMinimizer --freezesysts --fitresults fitresults_123456789.root
robselectron = 0.9778795
robselectronunc = 0.069453
robselectronexpunc = 0.066346
robselectronnosystunc = 0.006066


print "observed xs = %0.2f +/- %0.2f"%(mg5amc_nlo_fiducial_xs * robs,mg5amc_nlo_fiducial_xs * robsunc)

print "observed xs = %0.2f +/- %0.2f (stat) +/- %0.2f (exp) +/- %0.2f (theory)"%(mg5amc_nlo_fiducial_xs * robs,mg5amc_nlo_fiducial_xs * robsnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsexpunc,2)-pow(robsnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsunc,2)-pow(robsexpunc,2)))

print "observed xs = \sigma = %0.2f \\pm %0.2f \\text{ pb} = %0.2f \\pm %0.2f \\text{ (stat)} \\pm %0.2f \\text{ (exp)} \\pm %0.2f \\text{ (theory) pb}"%(mg5amc_nlo_fiducial_xs * robs,mg5amc_nlo_fiducial_xs * robsunc,mg5amc_nlo_fiducial_xs * robs,mg5amc_nlo_fiducial_xs * robsnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsexpunc,2)-pow(robsnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsunc,2)-pow(robsexpunc,2)))

print "observed xs based on electron channel = %0.2f +/- %0.2f"%(mg5amc_nlo_fiducial_xs * robselectron,mg5amc_nlo_fiducial_xs * robselectronunc)

print "observed xs based on electron channel = %0.2f +/- %0.2f (stat) +/- %0.2f (exp) +/- %0.2f (theory)"%(mg5amc_nlo_fiducial_xs * robselectron,mg5amc_nlo_fiducial_xs * robselectronnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(robselectronexpunc,2)-pow(robselectronnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(robselectronunc,2)-pow(robselectronexpunc,2)))

print "observed xs based on electron channel = \sigma = %0.2f \\pm %0.2f \\text{ pb} = %0.2f \\pm %0.2f \\text{ (stat)} \\pm %0.2f \\text{ (exp)} \\pm %0.2f \\text{ (theory) pb}"%(mg5amc_nlo_fiducial_xs * robselectron,mg5amc_nlo_fiducial_xs * robselectronunc,mg5amc_nlo_fiducial_xs * robselectron,mg5amc_nlo_fiducial_xs * robselectronnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(robselectronexpunc,2)-pow(robselectronnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(robselectronunc,2)-pow(robselectronexpunc,2)))

print "observed xs based on muon channel = %0.2f +/- %0.2f"%(mg5amc_nlo_fiducial_xs * robsmuon,mg5amc_nlo_fiducial_xs * robsmuonunc)

print "observed xs based on muon channel = %0.2f +/- %0.2f (stat) +/- %0.2f (exp) +/- %0.2f (theory)"%(mg5amc_nlo_fiducial_xs * robsmuon,mg5amc_nlo_fiducial_xs * robsmuonnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsmuonexpunc,2)-pow(robsmuonnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsmuonunc,2)-pow(robsmuonexpunc,2)))

print "observed xs based on muon channel = \sigma = %0.2f \\pm %0.2f \\text{ pb} = %0.2f \\pm %0.2f \\text{ (stat)} \\pm %0.2f \\text{ (exp)} \\pm %0.2f \\text{ (theory) pb}"%(mg5amc_nlo_fiducial_xs * robsmuon,mg5amc_nlo_fiducial_xs * robsmuonunc,mg5amc_nlo_fiducial_xs * robsmuon,mg5amc_nlo_fiducial_xs * robsmuonnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsmuonexpunc,2)-pow(robsmuonnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(robsmuonunc,2)-pow(robsmuonexpunc,2)))

print "xs = %0.2f +/- %0.2f"%(mg5amc_nlo_fiducial_xs * rexp,mg5amc_nlo_fiducial_xs * rexpunc)

print "xs = %0.2f +/- %0.2f (stat) +/- %0.2f (exp) +/- %0.2f (theory)"%(mg5amc_nlo_fiducial_xs * rexp,mg5amc_nlo_fiducial_xs * rexpnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpexpunc,2)-pow(rexpnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpunc,2)-pow(rexpexpunc,2)))

print "xs = \sigma = %0.2f \\pm %0.2f \\text{ pb} = %0.2f \\pm %0.2f \\text{ (stat)} \\pm %0.2f \\text{ (exp)} \\pm %0.2f \\text{ (theory) pb}"%(mg5amc_nlo_fiducial_xs * rexp,mg5amc_nlo_fiducial_xs * rexpunc,mg5amc_nlo_fiducial_xs * rexp,mg5amc_nlo_fiducial_xs * rexpnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpexpunc,2)-pow(rexpnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpunc,2)-pow(rexpexpunc,2)))

print "xs based on electron channel = %0.2f +/- %0.2f"%(mg5amc_nlo_fiducial_xs * rexpelectron,mg5amc_nlo_fiducial_xs * rexpelectronunc)

print "xs based on electron channel = %0.2f +/- %0.2f (stat) +/- %0.2f (exp) +/- %0.2f (theory)"%(mg5amc_nlo_fiducial_xs * rexpelectron,mg5amc_nlo_fiducial_xs * rexpelectronnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpelectronexpunc,2)-pow(rexpelectronnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpelectronunc,2)-pow(rexpelectronexpunc,2)))

print "xs based on electron channel = \sigma = %0.2f \\pm %0.2f \\text{ pb} = %0.2f \\pm %0.2f \\text{ (stat)} \\pm %0.2f \\text{ (exp)} \\pm %0.2f \\text{ (theory) pb}"%(mg5amc_nlo_fiducial_xs * rexpelectron,mg5amc_nlo_fiducial_xs * rexpelectronunc,mg5amc_nlo_fiducial_xs * rexpelectron,mg5amc_nlo_fiducial_xs * rexpelectronnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpelectronexpunc,2)-pow(rexpelectronnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpelectronunc,2)-pow(rexpelectronexpunc,2)))

print "xs based on muon channel = %0.2f +/- %0.2f"%(mg5amc_nlo_fiducial_xs * rexpmuon,mg5amc_nlo_fiducial_xs * rexpmuonunc)

print "xs based on muon channel = %0.2f +/- %0.2f (stat) +/- %0.2f (exp) +/- %0.2f (theory)"%(mg5amc_nlo_fiducial_xs * rexpmuon,mg5amc_nlo_fiducial_xs * rexpmuonnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpmuonexpunc,2)-pow(rexpmuonnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpmuonunc,2)-pow(rexpmuonexpunc,2)))

print "xs based on muon channel = \sigma = %0.2f \\pm %0.2f \\text{ pb} = %0.2f \\pm %0.2f \\text{ (stat)} \\pm %0.2f \\text{ (exp)} \\pm %0.2f \\text{ (theory) pb}"%(mg5amc_nlo_fiducial_xs * rexpmuon,mg5amc_nlo_fiducial_xs * rexpmuonunc,mg5amc_nlo_fiducial_xs * rexpmuon,mg5amc_nlo_fiducial_xs * rexpmuonnosystunc,mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpmuonexpunc,2)-pow(rexpmuonnosystunc,2)),mg5amc_nlo_fiducial_xs * math.sqrt(pow(rexpmuonunc,2)-pow(rexpmuonexpunc,2)))

