import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('infile')

args = parser.parse_args()

from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor

from  wgRealPhotonTemplateModule import *

from PhysicsTools.NanoAODTools.postprocessing.modules.common.countHistogramsModule import *
from PhysicsTools.NanoAODTools.postprocessing.modules.common.PrefireCorr import *
from PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer import *

p=PostProcessor(".",[args.infile],None,"wg_real_photon_template_keep_and_drop.txt",[countHistogramsModule(),wgRealPhotonTemplateModule(),puWeight_2018()],provenance=True,justcount=False,noOut=False,fwkJobReport=True,outputbranchsel = "wg_real_photon_template_output_branch_selection.txt")

p.run()

print "DONE"

