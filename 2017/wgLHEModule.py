import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

from PhysicsTools.NanoAODTools.postprocessing.tools import deltaR

class wgLHEProducer(Module):
    def __init__(self):
        pass
    def beginJob(self):
        pass
    def endJob(self):
        pass
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.out.branch("pass_lhe_selection",  "B");
        self.out.branch("lhe_lepton_charge",  "B");
        self.out.branch("lhe_lepton_pt",  "F");
        self.out.branch("lhe_lepton_phi",  "F");
        self.out.branch("lhe_lepton_eta",  "F");
        self.out.branch("lhe_lepton_mass",  "F");
        self.out.branch("lhe_photon_pt",  "F");
        self.out.branch("lhe_photon_phi",  "F");
        self.out.branch("lhe_photon_eta",  "F");
        self.out.branch("lhe_photon_mass",  "F");

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        prevdir = ROOT.gDirectory
        outputFile.cd()
        prevdir.cd()        
    def analyze(self, event):
        lheparts = Collection(event, "LHEPart")

        self.out.fillBranch("pass_lhe_selection",1)
            
        n_lhe_leptons = 0
        n_lhe_photons = 0

        for j in range(0,len(lheparts)):

            if abs(lheparts[j].pdgId) == 11 or abs(lheparts[j].pdgId) == 13 or abs(lheparts[j].pdgId) == 15:
                lhe_lepton_index = j
                n_lhe_leptons +=1

            if lheparts[j].pdgId == 22:
                if n_lhe_photons == 0:
                    lhe_photon_index = j
                n_lhe_photons +=1

        pass_lhe_selection = False

        if n_lhe_leptons >= 1 and n_lhe_photons >= 1:
            pass_lhe_selection = True

        if pass_lhe_selection: 
            self.out.fillBranch("pass_lhe_selection",1)
        else:    
            self.out.fillBranch("pass_lhe_selection",0)

        if n_lhe_leptons >= 1:
            self.out.fillBranch("lhe_lepton_pt",lheparts[lhe_lepton_index].pt)
            self.out.fillBranch("lhe_lepton_eta",lheparts[lhe_lepton_index].eta)
            self.out.fillBranch("lhe_lepton_phi",lheparts[lhe_lepton_index].phi)
            self.out.fillBranch("lhe_lepton_mass",lheparts[lhe_lepton_index].mass)
            if lheparts[lhe_lepton_index].pdgId > 0:
                self.out.fillBranch("lhe_lepton_charge",0)
            else:
                self.out.fillBranch("lhe_lepton_charge",1)
        else:    
            self.out.fillBranch("lhe_lepton_pt",0)
            self.out.fillBranch("lhe_lepton_eta",0)
            self.out.fillBranch("lhe_lepton_phi",0)
            self.out.fillBranch("lhe_lepton_mass",0)
        if n_lhe_photons >= 1:    
            self.out.fillBranch("lhe_photon_pt",lheparts[lhe_photon_index].pt)
            self.out.fillBranch("lhe_photon_eta",lheparts[lhe_photon_index].eta)
            self.out.fillBranch("lhe_photon_phi",lheparts[lhe_photon_index].phi)
            self.out.fillBranch("lhe_photon_mass",lheparts[lhe_photon_index].mass)
        else:    
            self.out.fillBranch("lhe_photon_pt",0)
            self.out.fillBranch("lhe_photon_eta",0)
            self.out.fillBranch("lhe_photon_phi",0)
            self.out.fillBranch("lhe_photon_mass",0)

        return True

wgLHEModule = lambda : wgLHEProducer() 
