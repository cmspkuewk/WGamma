date 2>&1 | tee wg_ewdim6_paper_plots.txt; python -u wg.py --lep both --ewdim6 --draw_ewdim6 -o /eos/user/a/amlevin/www/wg/ewdim6_paper_plots/   --phoeta both --year run2 --apply_2018_photon_phi_cut --paper 2>&1 | tee -a wg_ewdim6_paper_plots.txt; date 2>&1 | tee -a wg_ewdim6_paper_plots.txt

date 2>&1 | tee wg_muon_paper_plots.txt; python -u wg.py --lep muon -o /eos/user/a/amlevin/www/wg/muon_paper_plots/ --phoeta both --year run2  --float_sig_fake_cont --apply_2018_photon_phi_cut --make_datacard --paper  2>&1 | tee -a wg_muon_paper_plots.txt; date 2>&1 | tee -a wg_muon_paper_plots.txt

date 2>&1 | tee wg_electron_paper_plots.txt; python -u wg.py --lep electron -o /eos/user/a/amlevin/www/wg/electron_paper_plots/ --phoeta both --year run2  --float_sig_fake_cont --apply_2018_photon_phi_cut --make_datacard --paper  2>&1 | tee -a wg_electron_paper_plots.txt; date 2>&1 | tee -a wg_electron_paper_plots.txt


date 2>&1 | tee wg_muon_paper_table.txt; python -u wg.py --lep muon -o /dev/null --phoeta both --year run2  --float_sig_fake_cont --apply_2018_photon_phi_cut --make_datacard --paper --draw_non_fid 2>&1 | tee -a wg_muon_paper_table.txt; date 2>&1 | tee -a wg_muon_paper_table.txt

date 2>&1 | tee wg_electron_paper_table.txt; python -u wg.py --lep electron -o /dev/null --phoeta both --year run2  --float_sig_fake_cont --apply_2018_photon_phi_cut --make_datacard --paper --draw_non_fid 2>&1 | tee -a wg_electron_paper_table.txt; date 2>&1 | tee -a wg_electron_paper_table.txt


date 2>&1 | tee wg_ewdim6.txt; python -u wg.py --lep both --ewdim6 --draw_ewdim6 -o /eos/user/a/amlevin/www/wg/ewdim6/ --plots all  --phoeta both --year run2 --apply_2018_photon_phi_cut --make_unc_table 2>&1 | tee -a wg_ewdim6.txt; date 2>&1 | tee -a wg_ewdim6.txt

date 2>&1 | tee wg_muon.txt; python -u wg.py  --lep muon -o /eos/user/a/amlevin/www/wg/muon/ --phoeta both --year run2 --make_datacard --float_sig_fake_cont --plots all --apply_2018_photon_phi_cut --make_unc_table 2>&1 | tee -a wg_muon.txt; date 2>&1 | tee -a wg_muon.txt

date 2>&1 | tee wg_electron.txt; python -u wg.py --lep electron -o /eos/user/a/amlevin/www/wg/electron/ --phoeta both --year run2 --make_datacard --float_sig_fake_cont --plots all --apply_2018_photon_phi_cut --make_unc_table 2>&1 | tee -a wg_electron.txt; date 2>&1 | tee -a wg_electron.txt



date 2>&1 | tee wg_electron_zveto.txt; python -u wg.py --lep electron -o /eos/user/a/amlevin/www/wg/electron_zveto/ --phoeta both --year run2 --plots all --zveto --apply_2018_photon_phi_cut 2>&1 | tee -a wg_electron_zveto.txt; date 2>&1 | tee -a wg_electron_zveto.txt

date 2>&1 | tee wg_electron_zveto_nophotonphicut.txt; python -u wg.py --lep electron -o /eos/user/a/amlevin/www/wg/electron_zveto_nophotonphicut/ --phoeta both --year run2 --plots all --zveto 2>&1 | tee -a wg_electron_zveto_nophotonphicut.txt; date 2>&1 | tee -a wg_electron_zveto_nophotonphi.txt

date 2>&1 | tee wg_muon_nophotonphicut.txt; python -u wg.py  --lep muon -o /eos/user/a/amlevin/www/wg/muon_nophotonphicut/ --phoeta both --year run2 --plots all 2>&1 | tee -a wg_muon_nophotonphicut.txt; date 2>&1 | tee -a wg_muon_nophotonphicut.txt

date 2>&1 | tee wg_electron_mcfakephoton_zveto.txt; python -u wg.py  --lep electron -o /eos/user/a/amlevin/www/wg/electron_mcfakephoton_zveto/ --phoeta both --year run2 --plots all --zveto --use_wjets_for_fake_photon --apply_2018_photon_phi_cut 2>&1 | tee -a wg_electron_mcfakephoton_zveto.txt; date 2>&1 | tee -a wg_electron_mcfakephoton_zveto.txt

date 2>&1 | tee wg_muon_mcfakephoton.txt; python -u wg.py --lep muon -o /eos/user/a/amlevin/www/wg/muon_mcfakephoton/ --phoeta both --year run2 --plots all --use_wjets_for_fake_photon --apply_2018_photon_phi_cut 2>&1 | tee -a wg_muon_mcfakephoton.txt; date 2>&1 | tee -a wg_muon_mcfakephoton.txt
