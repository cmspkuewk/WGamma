import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--nthreads',dest='nthreads',default=0) #the argument to EnableImplicitMT                                                                  
parser.add_argument('--year',dest='year',default="run2") #the argument to EnableImplicitMT                                                                  
parser.add_argument('--workdir',dest='workdir',default='/eos/user/a/amlevin')
args = parser.parse_args()

ROOT.ROOT.EnableImplicitMT(int(args.nthreads))

dict_lumi = {"2016" : 35.9, "2017" : 41.5, "2018" : 59.6}

if args.year == "2016":
    years = ["2016"]
    totallumi=dict_lumi["2016"]
elif args.year == "2017":
    years=["2017"]
    totallumi=dict_lumi["2017"]
elif args.year == "2018":
    years=["2018"]
    totallumi=dict_lumi["2018"]
elif args.year == "run2":
    years=["2016","2017","2018"]
    totallumi=dict_lumi["2016"]+dict_lumi["2017"]+dict_lumi["2018"]
else:
    assert(0)

import math

#matrix_plus_lo_xs = 3*9135./1000

#matrix_plus_nlo_xs = 3*1.608e+04/1000

#matrix_plus_nnlo_xs = 3*2.007e+04/1000

#matrix_minus_lo_xs = 3*7997./1000

#matrix_minus_nlo_xs = 3*1.476e+04/1000

#matrix_minus_nnlo_xs = 3*1.652e+04/1000

matrix_plus_lo_xs = 3*1011./1000

matrix_plus_nlo_xs = 3*3216./1000

matrix_plus_nnlo_xs = 3*4107./1000

matrix_minus_lo_xs = 3*816.1/1000

matrix_minus_nlo_xs = 3*2969./1000

matrix_minus_nnlo_xs = 3*3673./1000

matrix_lo_xs = matrix_plus_lo_xs+matrix_minus_lo_xs

matrix_nlo_xs = matrix_plus_nlo_xs+matrix_minus_nlo_xs

matrix_nnlo_xs = matrix_plus_nnlo_xs+matrix_minus_nnlo_xs

variable_definitions = [
    ["detalg" , "abs(lhe_lepton_eta-lhe_photon_eta)"],
    ["dphilg" , "abs(lhe_lepton_phi - lhe_photon_phi) > TMath::Pi() ? abs(abs(lhe_lepton_phi - lhe_photon_phi) - 2*TMath::Pi()) : abs(lhe_lepton_phi - lhe_photon_phi)"],
    ["drlg" , "sqrt(dphilg*dphilg+detalg*detalg)" ],
]

fid_reg_cuts = "(pass_lhe_selection && drlg > 0.5 && lhe_lepton_pt > 25 && lhe_photon_pt > 25 && abs(lhe_lepton_eta) < 2.5 && abs(lhe_photon_eta) < 2.5)"

dict_mg5amc_xs = {"2016" : 178.6, "2017" : 190.9, "2018" : 190.9}

mg5amc_lhe_xs = float(0)

for year in years:

    mg5amc_wgjets_filename = args.workdir+"/data/wg/"+year+"/1June2019jetunc/wgjets.root"

    mg5amc_rdf = ROOT.RDataFrame("Events",mg5amc_wgjets_filename)  

    mg5amc_wgjets_file =  ROOT.TFile(mg5amc_wgjets_filename)

    for variable_definition in variable_definitions:
        mg5amc_rdf  = mg5amc_rdf.Define(variable_definition[0],variable_definition[1])

    mg5amc_xs = dict_mg5amc_xs[year]

    mg5amc_lhe_xs += mg5amc_xs*dict_lumi[year]*mg5amc_rdf.Filter(fid_reg_cuts).Define("weight","gen_weight/abs(gen_weight)").Sum("weight").GetValue()/mg5amc_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)

mg5amc_lhe_xs/=totallumi

print "mg5amc lhe xs: \\sigma = %.2f \\text{ pb}"%(mg5amc_lhe_xs)

print "matrix lo xs: \\sigma = %.2f \\text{ pb}"%(matrix_lo_xs)

print "matrix nlo xs: \\sigma = %.2f \\text{ pb}"%(matrix_nlo_xs)

print "matrix nnlo xs: \\sigma = %.2f \\text{ pb}"%(matrix_nnlo_xs)
