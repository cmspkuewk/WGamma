import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-o","--outdir",type=str,required=True)

args = parser.parse_args()

import ROOT

photon_id_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/Fall17V2_2016_Medium_photons.root");
photon_id_2016_sf = photon_id_2016_sf_file.Get("EGamma_SF2D");

photon_id_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/2017_PhotonsMedium.root");
photon_id_2017_sf = photon_id_2017_sf_file.Get("EGamma_SF2D");

photon_id_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/2018_PhotonsMedium.root","read");
photon_id_2018_sf = photon_id_2018_sf_file.Get("EGamma_SF2D");

electron_id_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/2016LegacyReReco_ElectronMedium_Fall17V2.root","read");
electron_id_2016_sf = electron_id_2016_sf_file.Get("EGamma_SF2D");

electron_id_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/2017_ElectronMedium.root","read");
electron_id_2017_sf = electron_id_2017_sf_file.Get("EGamma_SF2D");

electron_id_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/2018_ElectronMedium.root","read");
electron_id_2018_sf = electron_id_2018_sf_file.Get("EGamma_SF2D");

electron_reco_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/EGM2D_BtoH_GT20GeV_RecoSF_Legacy2016.root","read");
electron_reco_2016_sf = electron_reco_2016_sf_file.Get("EGamma_SF2D");

electron_reco_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/egammaEffi.txt_EGM2D_runBCDEF_passingRECO.root","read");
electron_reco_2017_sf = electron_reco_2017_sf_file.Get("EGamma_SF2D");

electron_reco_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/egammaEffi.txt_EGM2D_updatedAll.root" ,"read");
electron_reco_2018_sf = electron_reco_2018_sf_file.Get("EGamma_SF2D");

electron_hlt_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/electron_hlt_sfs_2016.root","read");
electron_hlt_2016_sf = electron_hlt_2016_sf_file.Get("EGamma_SF2D");

electron_hlt_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/electron_hlt_sfs_2017.root","read");
electron_hlt_2017_sf = electron_hlt_2017_sf_file.Get("EGamma_SF2D");

electron_hlt_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/electron_hlt_sfs_2018.root" ,"read");
electron_hlt_2018_sf = electron_hlt_2018_sf_file.Get("EGamma_SF2D");

muon_iso_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/RunBCDEF_SF_ISO.root","read");
muon_iso_2016_sf = muon_iso_2016_sf_file.Get("NUM_TightRelIso_DEN_TightIDandIPCut_eta_pt");

muon_id_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/RunBCDEF_SF_ID.root","read");
muon_id_2016_sf = muon_id_2016_sf_file.Get("NUM_TightID_DEN_genTracks_eta_pt");

muon_iso_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/RunBCDEF_SF_ISO.root","read");
muon_iso_2017_sf = muon_iso_2017_sf_file.Get("NUM_TightRelIso_DEN_TightIDandIPCut_pt_abseta");

muon_id_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/RunBCDEF_SF_ID.root","read");
muon_id_2017_sf = muon_id_2017_sf_file.Get("NUM_TightID_DEN_genTracks_pt_abseta");

muon_iso_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/RunABCD_SF_ISO.root","read");
muon_iso_2018_sf = muon_iso_2018_sf_file.Get("NUM_TightRelIso_DEN_TightIDandIPCut_pt_abseta");

muon_id_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/RunABCD_SF_ID.root","read");
muon_id_2018_sf = muon_id_2018_sf_file.Get("NUM_TightID_DEN_TrackerMuons_pt_abseta");

muon_hlt_2016_sf_file = ROOT.TFile.Open("eff_scale_factors/2016/EfficienciesStudies_2016_trigger_EfficienciesAndSF_RunGtoH.root","read");
muon_hlt_2016_sf = muon_hlt_2016_sf_file.Get("IsoMu24_OR_IsoTkMu24_PtEtaBins/abseta_pt_ratio");

muon_hlt_2017_sf_file = ROOT.TFile.Open("eff_scale_factors/2017/EfficienciesAndSF_RunBtoF_Nov17Nov2017.root","read");
muon_hlt_2017_sf = muon_hlt_2017_sf_file.Get("IsoMu27_PtEtaBins/abseta_pt_ratio");

muon_hlt_2018_sf_file = ROOT.TFile.Open("eff_scale_factors/2018/EfficienciesStudies_2018_trigger_EfficienciesAndSF_2018Data_BeforeMuonHLTUpdate.root","read");
muon_hlt_2018_sf = muon_hlt_2018_sf_file.Get("IsoMu24_PtEtaBins/abseta_pt_ratio");

c = ROOT.TCanvas("c","c")

c.SetLogy(1)

photon_id_2016_sf.SetTitle("2016 Photon ID Scale Factors")
photon_id_2016_sf.SetStats(0)
photon_id_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
photon_id_2016_sf.GetXaxis().SetTitle("\eta")

photon_id_2016_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/photon_id_sf_2016.png")

photon_id_2017_sf.SetTitle("2017 Photon ID Scale Factors")
photon_id_2017_sf.SetStats(0)
photon_id_2017_sf.GetYaxis().SetTitle("p_{T} (GeV)")
photon_id_2017_sf.GetXaxis().SetTitle("\eta")

photon_id_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/photon_id_sf_2017.png")

photon_id_2018_sf.SetTitle("2018 Photon ID Scale Factors")
photon_id_2018_sf.SetStats(0)
photon_id_2018_sf.GetYaxis().SetTitle("p_{T} (GeV)")
photon_id_2018_sf.GetXaxis().SetTitle("\eta")

photon_id_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/photon_id_sf_2018.png")

c.SetLogy(0)

c.SetLogy(1)

electron_hlt_2016_sf.SetTitle("2016 Electron HLT Scale Factors")
electron_hlt_2016_sf.SetStats(0)
electron_hlt_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_hlt_2016_sf.GetXaxis().SetTitle("\eta")

electron_hlt_2016_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_hlt_sf_2016.png")

electron_hlt_2017_sf.SetTitle("2017 Electron HLT Scale Factors")
electron_hlt_2017_sf.SetStats(0)
electron_hlt_2017_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_hlt_2017_sf.GetXaxis().SetTitle("\eta")

electron_hlt_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_hlt_sf_2017.png")

electron_hlt_2018_sf.SetTitle("2018 Electron HLT Scale Factors")
electron_hlt_2018_sf.SetStats(0)
electron_hlt_2018_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_hlt_2018_sf.GetXaxis().SetTitle("\eta")

electron_hlt_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_hlt_sf_2018.png")

c.SetLogy(0)

c.SetLogy(1)

electron_id_2016_sf.SetTitle("2016 Electron ID and Iso Scale Factors")
electron_id_2016_sf.SetStats(0)
electron_id_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_id_2016_sf.GetXaxis().SetTitle("\eta")

electron_id_2016_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_id_sf_2016.png")

electron_id_2017_sf.SetTitle("2017 Electron ID and Iso Scale Factors")
electron_id_2017_sf.SetStats(0)
electron_id_2017_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_id_2017_sf.GetXaxis().SetTitle("\eta")

electron_id_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_id_sf_2017.png")

electron_id_2018_sf.SetTitle("2018 Electron ID and Iso Scale Factors")
electron_id_2018_sf.SetStats(0)
electron_id_2018_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_id_2018_sf.GetXaxis().SetTitle("\eta")

electron_id_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_id_sf_2018.png")

c.SetLogy(0)

electron_reco_2016_sf.SetTitle("2016 Electron Reco Scale Factors")
electron_reco_2016_sf.SetStats(0)
electron_reco_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_reco_2016_sf.GetXaxis().SetTitle("\eta")

electron_reco_2016_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_reco_sf_2016.png")

electron_reco_2017_sf.SetTitle("2017 Electron Reco Scale Factors")
electron_reco_2017_sf.SetStats(0)
electron_reco_2017_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_reco_2017_sf.GetXaxis().SetTitle("\eta")

electron_reco_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_reco_sf_2017.png")

electron_reco_2018_sf.SetTitle("2018 Electron Reco Scale Factors")
electron_reco_2018_sf.SetStats(0)
electron_reco_2018_sf.GetYaxis().SetTitle("p_{T} (GeV)")
electron_reco_2018_sf.GetXaxis().SetTitle("\eta")

electron_reco_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/electron_reco_sf_2018.png")

c.SetLogy(1)

muon_hlt_2016_sf.SetTitle("2016 Muon HLT Scale Factors")
muon_hlt_2016_sf.SetStats(0)
muon_hlt_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
muon_hlt_2016_sf.GetXaxis().SetTitle("\eta")
muon_hlt_2016_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_hlt_sf_2016.png")

muon_hlt_2017_sf.SetTitle("2017 Muon HLT Scale Factors")
muon_hlt_2017_sf.SetStats(0)
muon_hlt_2017_sf.GetYaxis().SetTitle("p_{T} (GeV)")
muon_hlt_2017_sf.GetXaxis().SetTitle("\eta")

muon_hlt_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_hlt_sf_2017.png")

muon_hlt_2018_sf.SetTitle("2018 Muon HLT Scale Factors")
muon_hlt_2018_sf.SetStats(0)
muon_hlt_2018_sf.GetYaxis().SetTitle("p_{T} (GeV)")
muon_hlt_2018_sf.GetXaxis().SetTitle("\eta")

muon_hlt_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_hlt_sf_2018.png")

c.SetLogy(0)

c.SetLogy(1)

muon_id_2016_sf.SetTitle("2016 Muon ID Scale Factors")
muon_id_2016_sf.SetStats(0)
muon_id_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
muon_id_2016_sf.GetXaxis().SetTitle("\eta")

muon_id_2016_sf.Draw("colz text55")

c.SaveAs(args.outdir+"/muon_id_sf_2016.png")

c.SetLogy(0)

muon_id_2017_sf.SetTitle("2017 Muon ID Scale Factors")
muon_id_2017_sf.SetStats(0)
muon_id_2017_sf.GetXaxis().SetTitle("p_{T} (GeV)")
muon_id_2017_sf.GetYaxis().SetTitle("\eta")

muon_id_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_id_sf_2017.png")

muon_id_2018_sf.SetTitle("2018 Muon ID Scale Factors")
muon_id_2018_sf.SetStats(0)
muon_id_2018_sf.GetXaxis().SetTitle("p_{T} (GeV)")
muon_id_2018_sf.GetYaxis().SetTitle("\eta")

muon_id_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_id_sf_2018.png")

c.SetLogy(1)

muon_iso_2016_sf.SetTitle("2016 Muon Iso Scale Factors")
muon_iso_2016_sf.SetStats(0)
muon_iso_2016_sf.GetYaxis().SetTitle("p_{T} (GeV)")
muon_iso_2016_sf.GetXaxis().SetTitle("\eta")

muon_iso_2016_sf.Draw("colz text55")

c.SaveAs(args.outdir+"/muon_iso_sf_2016.png")

c.SetLogy(0)

muon_iso_2017_sf.SetTitle("2017 Muon Iso Scale Factors")
muon_iso_2017_sf.SetStats(0)
muon_iso_2017_sf.GetXaxis().SetTitle("p_{T} (GeV)")
muon_iso_2017_sf.GetYaxis().SetTitle("\eta")

muon_iso_2017_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_iso_sf_2017.png")

muon_iso_2018_sf.SetTitle("2018 Muon Iso Scale Factors")
muon_iso_2018_sf.SetStats(0)
muon_iso_2018_sf.GetXaxis().SetTitle("p_{T} (GeV)")
muon_iso_2018_sf.GetYaxis().SetTitle("\eta")

muon_iso_2018_sf.Draw("colz text45")

c.SaveAs(args.outdir+"/muon_iso_sf_2018.png")

