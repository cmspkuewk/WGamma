import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--nthreads',dest='nthreads',default=0) #the argument to EnableImplicitMT                                                                  
parser.add_argument('--year',dest='year',default="run2") #the argument to EnableImplicitMT                                                                  
parser.add_argument('--workdir',dest='workdir',default='/eos/user/a/amlevin')
args = parser.parse_args()

ROOT.ROOT.EnableImplicitMT(int(args.nthreads))

dict_lumi = {"2016" : 35.9, "2017" : 41.5, "2018" : 59.6}

if args.year == "2016":
    years = ["2016"]
    totallumi=dict_lumi["2016"]
elif args.year == "2017":
    years=["2017"]
    totallumi=dict_lumi["2017"]
elif args.year == "2018":
    years=["2018"]
    totallumi=dict_lumi["2018"]
elif args.year == "run2":
    years=["2016","2017","2018"]
    totallumi=dict_lumi["2016"]+dict_lumi["2017"]+dict_lumi["2018"]
else:
    assert(0)

import math

fid_reg_cuts = "(pass_fid_selection && fid_met_pt > 0)"

dict_mg5amc_xs = {"2016" : 178.6, "2017" : 190.9, "2018" : 190.9}

dict_powheg_plus_xs = {"2016" : 33420.0, "2017" : 34220. , "2018" : 34220.}

dict_powheg_minus_xs = {"2016" : 24780.0, "2017" :  25350., "2018" : 25350.}

mg5amc_fiducial_xs = float(0)

powheg_fiducial_xs = float(0)

mg5amc_fiducial_xs_scale_variation = []

mg5amc_fiducial_xs_pdf_variation = []

powheg_fiducial_xs_scale_variation = []

powheg_fiducial_xs_pdf_variation = []

for i in range(0,8):
    mg5amc_fiducial_xs_scale_variation.append(float(0))
    powheg_fiducial_xs_scale_variation.append(float(0))

for i in range(0,32):
    mg5amc_fiducial_xs_pdf_variation.append(float(0))
    powheg_fiducial_xs_pdf_variation.append(float(0))

for year in years:

    mg5amc_wgjets_filename = args.workdir+"/data/wg/"+year+"/1June2019/wgjets.root"

    mg5amc_wgjets_file =  ROOT.TFile(mg5amc_wgjets_filename)

    mg5amc_xs = dict_mg5amc_xs[year]

    powheg_plus_xs = dict_powheg_plus_xs[year]

    powheg_minus_xs = dict_powheg_minus_xs[year]

    powheg_plus_wgjets_filename = args.workdir+"/data/wg/"+year+"/1June2019/powhegwplusg.root"
    powheg_minus_wgjets_filename = args.workdir+"/data/wg/"+year+"/1June2019/powhegwminusg.root"

    powheg_plus_wgjets_file =  ROOT.TFile(powheg_plus_wgjets_filename)
    powheg_minus_wgjets_file =  ROOT.TFile(powheg_minus_wgjets_filename)

    n_weighted_powheg_plus = powheg_plus_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)
    n_weighted_powheg_minus = powheg_minus_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)

    n_weighted_powheg_plus_pass_gen_selection = powheg_plus_wgjets_file.Get("Events").GetEntries(fid_reg_cuts + " && gen_weight > 0") - powheg_plus_wgjets_file.Get("Events").GetEntries(fid_reg_cuts + " && gen_weight < 0")
    n_weighted_powheg_minus_pass_gen_selection = powheg_minus_wgjets_file.Get("Events").GetEntries(fid_reg_cuts+ " && gen_weight > 0") - powheg_minus_wgjets_file.Get("Events").GetEntries(fid_reg_cuts+ " && gen_weight < 0")

    mg5amc_fiducial_xs +=  mg5amc_xs*dict_lumi[year]*(mg5amc_wgjets_file.Get("Events").GetEntries(fid_reg_cuts + " && gen_weight > 0") - mg5amc_wgjets_file.Get("Events").GetEntries(fid_reg_cuts + " && gen_weight < 0"))/mg5amc_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)

    powheg_fiducial_xs += powheg_plus_xs*dict_lumi[year]*n_weighted_powheg_plus_pass_gen_selection/n_weighted_powheg_plus + powheg_minus_xs*dict_lumi[year]*n_weighted_powheg_minus_pass_gen_selection/n_weighted_powheg_minus

    mg5amc_rdf = ROOT.RDataFrame("Events",mg5amc_wgjets_filename)    

    mg5mc_rdf_fid = mg5amc_rdf.Filter(fid_reg_cuts)

    powheg_plus_wgjets_rdf = ROOT.RDataFrame("Events",powheg_plus_wgjets_filename)    

    powheg_plus_wgjets_rdf_fid = powheg_plus_wgjets_rdf.Filter(fid_reg_cuts)

    powheg_minus_wgjets_rdf = ROOT.RDataFrame("Events",powheg_minus_wgjets_filename)    

    powheg_minus_wgjets_rdf_fid = powheg_minus_wgjets_rdf.Filter(fid_reg_cuts)

    mg5amc_neventsgenweight_passfidselection_scaleweight = []

    mg5amc_neventsgenweight_passfidselection_pdfweight = []

    powheg_plus_wgjets_neventsgenweight_passfidselection_scaleweight = []

    powheg_minus_wgjets_neventsgenweight_passfidselection_scaleweight = []

    powheg_plus_wgjets_neventsgenweight_passfidselection_pdfweight = []

    powheg_minus_wgjets_neventsgenweight_passfidselection_pdfweight = []

    for i in range(0,8):

        if year == "2016":
            mg5amc_neventsgenweight_passfidselection_scaleweight.append(mg5mc_rdf_fid.Define("weight","LHEScaleWeight["+str(i)+"]*2*gen_weight/abs(gen_weight)").Sum("weight"))
        else:    
            mg5amc_neventsgenweight_passfidselection_scaleweight.append(mg5mc_rdf_fid.Define("weight","LHEScaleWeight["+str(i)+"]*gen_weight/abs(gen_weight)").Sum("weight"))

        powheg_plus_wgjets_neventsgenweight_passfidselection_scaleweight.append(powheg_plus_wgjets_rdf_fid.Define("weight","LHEScaleWeight["+str(i)+"]*gen_weight/abs(gen_weight)").Sum("weight"))

        powheg_minus_wgjets_neventsgenweight_passfidselection_scaleweight.append(powheg_minus_wgjets_rdf_fid.Define("weight","LHEScaleWeight["+str(i)+"]*gen_weight/abs(gen_weight)").Sum("weight"))

    for i in range(0,32):
        mg5amc_rdf = ROOT.RDataFrame("Events",mg5amc_wgjets_filename)    

        if year == "2016":
            mg5amc_neventsgenweight_passfidselection_pdfweight.append(mg5mc_rdf_fid.Define("weight","LHEPdfWeight["+str(i)+"]*2*gen_weight/abs(gen_weight)").Sum("weight"))
        else:    
            mg5amc_neventsgenweight_passfidselection_pdfweight.append(mg5mc_rdf_fid.Define("weight","LHEPdfWeight["+str(i)+"]*gen_weight/abs(gen_weight)").Sum("weight"))

        powheg_plus_wgjets_neventsgenweight_passfidselection_pdfweight.append(powheg_plus_wgjets_rdf_fid.Define("weight","LHEPdfWeight["+str(i)+"]*gen_weight/abs(gen_weight)").Sum("weight"))

        powheg_minus_wgjets_neventsgenweight_passfidselection_pdfweight.append(powheg_minus_wgjets_rdf_fid.Define("weight","LHEPdfWeight["+str(i)+"]*gen_weight/abs(gen_weight)").Sum("weight"))



    for i in range(0,8):
        mg5amc_fiducial_xs_scale_variation[i]+=mg5amc_xs*dict_lumi[year]*mg5amc_neventsgenweight_passfidselection_scaleweight[i].GetValue()/mg5amc_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)
        powheg_fiducial_xs_scale_variation[i]+=powheg_plus_xs*dict_lumi[year]*3*powheg_plus_wgjets_neventsgenweight_passfidselection_scaleweight[i].GetValue()/powheg_plus_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)+powheg_minus_xs*dict_lumi[year]*3*powheg_minus_wgjets_neventsgenweight_passfidselection_scaleweight[i].GetValue()/powheg_minus_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1) 

    for i in range(0,32):
        mg5amc_fiducial_xs_pdf_variation[i]+=mg5amc_xs*dict_lumi[year]*mg5amc_neventsgenweight_passfidselection_pdfweight[i].GetValue()/mg5amc_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)
        powheg_fiducial_xs_pdf_variation[i]+=powheg_plus_xs*dict_lumi[year]*3*powheg_plus_wgjets_neventsgenweight_passfidselection_pdfweight[i].GetValue()/powheg_plus_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)+powheg_minus_xs*dict_lumi[year]*3*powheg_minus_wgjets_neventsgenweight_passfidselection_pdfweight[i].GetValue()/powheg_minus_wgjets_file.Get("nEventsGenWeighted").GetBinContent(1)

mg5amc_fiducial_xs/=totallumi
powheg_fiducial_xs/=totallumi

for i in range(0,8):
    mg5amc_fiducial_xs_scale_variation[i]/=totallumi
    powheg_fiducial_xs_scale_variation[i]/=totallumi

for i in range(0,32):
    mg5amc_fiducial_xs_pdf_variation[i]/=totallumi
    powheg_fiducial_xs_pdf_variation[i]/=totallumi

mg5amc_fiducial_xs_scale_unc = -1
powheg_fiducial_xs_scale_unc = -1

for i in range(0,8):

    if i == 2 or i == 5:
        continue

    if abs(mg5amc_fiducial_xs_scale_variation[i] - mg5amc_fiducial_xs) > mg5amc_fiducial_xs_scale_unc:
        mg5amc_fiducial_xs_scale_unc = abs(mg5amc_fiducial_xs_scale_variation[i] - mg5amc_fiducial_xs)

    if abs(powheg_fiducial_xs_scale_variation[i] - powheg_fiducial_xs) > powheg_fiducial_xs_scale_unc:
        powheg_fiducial_xs_scale_unc = abs(powheg_fiducial_xs_scale_variation[i] - powheg_fiducial_xs)

assert(mg5amc_fiducial_xs_scale_unc != -1)
assert(powheg_fiducial_xs_scale_unc != -1)

mg5amc_fiducial_xs_mean_pdf=0
powheg_fiducial_xs_mean_pdf=0

for i in range(0,32):
    mg5amc_fiducial_xs_mean_pdf += mg5amc_fiducial_xs_pdf_variation[i]
    powheg_fiducial_xs_mean_pdf += powheg_fiducial_xs_pdf_variation[i]

mg5amc_fiducial_xs_mean_pdf = mg5amc_fiducial_xs_mean_pdf/32
powheg_fiducial_xs_mean_pdf = powheg_fiducial_xs_mean_pdf/32

mg5amc_fiducial_xs_stddev_pdf = 0
powheg_fiducial_xs_stddev_pdf = 0

for i in range(0,32):
    mg5amc_fiducial_xs_stddev_pdf += pow(mg5amc_fiducial_xs_pdf_variation[i] - mg5amc_fiducial_xs_mean_pdf,2)
    powheg_fiducial_xs_stddev_pdf += pow(powheg_fiducial_xs_pdf_variation[i] - powheg_fiducial_xs_mean_pdf,2)

mg5amc_fiducial_xs_stddev_pdf = math.sqrt(mg5amc_fiducial_xs_stddev_pdf/(32-1))
powheg_fiducial_xs_stddev_pdf = math.sqrt(powheg_fiducial_xs_stddev_pdf/(32-1))

print "powheg fiducial xs: \\sigma = %.1f \pm %.1f \\text{ (scale) \pm %.1f \\text{ (PDF) pb}"%(powheg_fiducial_xs,powheg_fiducial_xs_scale_unc,powheg_fiducial_xs_stddev_pdf)
print "mg5amc fiducial xs: \\sigma = %.1f \pm %.1f \\text{ (scale) \pm %.1f \\text{ (PDF) pb}"%(mg5amc_fiducial_xs,mg5amc_fiducial_xs_scale_unc,mg5amc_fiducial_xs_stddev_pdf)
